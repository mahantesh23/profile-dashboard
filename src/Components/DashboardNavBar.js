
/* Fixed-top-Navigation Bars */

import React, { Component } from 'react';
import './Nav.css';

class DashboardNavBar extends Component {

  render() {

    return (

        /* Top Header Navigation   */

            <div class="ember-view">


                <div className="sub-header ember-view">

                    <nav className="navbar top-nav-bar">

                        <div  className="collapse navbar-collapse">
                            <div className="navbar-header">
                                <a className="navbar-brand active ember-view">
                                    <div className="logo-wrapper">
                                        <img alt="Talent Center"
                                             src="https://assets.freshteam.com/production/assets/images/brand-header-logo-639ee6b2d4f225286864001ded5ff546.png"
                                             onError="this.onerror=null;this.src='https://assets.freshteam.com/production/assets/images/brand-header-logo-639ee6b2d4f225286864001ded5ff546.png';"
                                             className="brand-header-logo" />
                                    </div>
                                </a>
                                    <h2 className="brand-name">Talent Center</h2>
                            </div>

                                <ul className="nav navbar-nav navbar-right">
                                <li className="common-search-button">

                                    <a>  <i className="icon-search"></i> </a>

                                </li>

                                <li className="page-common-add dropdown ember-view">
                                    <a className="btn add-new-button dropdown-toggle ember-view">
                                        <i className="icon-add"></i> New
                                    </a>
                                </li>

                                <li className="top-nav-separator"></li>

                                <li className="notification dropdown ember-view"><a className="dropdown-toggle ember-view">
                                    <i className="icon-bell"></i>
                                </a>


                                </li>
                                <li className="profile-avatar-click-dropdown dropdown ember-view">
                                    <a className="header-profile-icon dropdown-toggle ember-view">
                                    <div className="vertical-align ember-view">
                                        <div className="avatar-circle">
                                            <div id="ember1247" className="initialsAvatar avatarColor-21 ember-view"> P
                                            </div>
                                        </div>
                                        </div>
                                </a>
                                </li>
                                </ul>
                        </div>
                        <div className="subheader-content">
                            <div className="subheader-left">

                                <ol className="breadcrumb ember-view">
                                    <li className="ember-view">
                                        <a className="active ember-view"> Dashboard</a>
                                    </li>
                                </ol>

                                <div className="sub-header-btns"></div>
                            </div>
                        </div>
                    </nav>

                </div>
            </div>
        );
    }
}

export default DashboardNavBar;
