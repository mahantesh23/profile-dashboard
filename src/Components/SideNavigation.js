/* Sticky Side Navigation Bar */

import React, { Component } from 'react';
import './Nav.css';

class SideNavigation extends Component {


  render() {
    return (

            <div>
                <nav className="navbar primary-nav-bar navbar-inverse">

                    <div className="ft-logo "> <i className="icon-ft"></i></div>

                    <ul className="nav nav-pills nav-stacked">

                        <li><a data-hint="Dashboard" href="/"  className="hint--right active ember-view">
                            <i className="icon-dashboard"></i> </a>
                        </li>

                        <li><a data-hint="Recruit" href="/hire" className="hint--right ember-view">
                            <i className="icon-recruit"></i></a>
                        </li>

                        <li><a data-hint="Conversations" href="/conversations" className="hint--right ember-view">
                            <i className="icon-conversation"></i></a>
                        </li>

                        <li><a data-hint="Tasks" href="/tasks"  className="hint--right ember-view">
                            <i className="icon-task"></i></a>
                        </li>

                        <li><a data-hint="Employee Directory" href="/employees" className="hint--right ember-view">
                            <i className="icon-directory"></i></a>
                        </li>

                        <li><a data-hint="Reports" href="/reports"  className="hint--right ember-view">
                            <i className="icon-reports-nav"></i></a>
                        </li>

                        <li className="bottom"><a data-hint="Settings" href="/settings" className="hint--right nav-settings ember-view ">
                            <i className="icon-settings"></i></a>
                        </li>


                        <li className="page-help ember-view ">
                            <a  className="hint--right">
                            <i className="icon-help-circled"></i>
                            </a>
                        </li>

                        <li >
                                <a className="hint--right">
                                <i className="icon-comment"></i>
                                <div className="unread-msg"></div>
                                </a>
                        </li>

                        <li className="ominbar-btn">
                            <div className="freshworks-omnibar-event-target" data-omnibar-event-target=""
                                 data-omnibar-region="US" data-omnibar-user-role="admin"
                                 data-omnibar-environment="production" data-omnibar-product="freshteam">
                            </div>

                        </li>

                    </ul>
                </nav>

            </div>

      );
  }
}

export default SideNavigation;
