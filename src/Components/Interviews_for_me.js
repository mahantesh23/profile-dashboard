import React, { Component } from 'react';
import './Nav.css';

class Interviews_for_me extends Component {


  render() {
    return (
        <div>
                            <div className="row mt20">
                                    <div className="col-xs-12">
                                        <div  className="ember-view">
                                            <div className="clearfix">
                                                <h2 className="dashboard-title col-xs-4 mb0 pl0">
                                                    Interviews
                                                    <div  className="vertical-align dropdown ember-view">
                                                        <a href="#" className="dashboard-dropdown dropdown-toggle ember-view"> For Me
                                                        </a>

                                                    </div>
                                                </h2>
                                                <div className="col-xs-8">
                                                    <ul className="pull-right dashboard-tab line-nav nav nav-tabs">

                                                        <li>

                                                                Feedback due

                                                        </li>
                                                        <li className="active">
                                                            <a>Today</a>
                                                        </li>

                                                        <li>Upcoming</li>

                                                        <li>Completed</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div className="white-bg interview-block">
                                                <div className="blocks-header">
                                                    <div className="row">
                                                        <div className="col-xs-3">
                                                            Candidate
                                                        </div>
                                                        <div className="col-xs-3">
                                                            Interviewer
                                                        </div>
                                                        <div className="col-xs-3">
                                                            Schedule info
                                                        </div>
                                                        <div className="col-xs-3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="ember-view">
                                                    <div className="ps-content blocks-content interviews ps-container ps-theme-default">
                                                        <div className="tab-content">
                                                            <ul className="list-unstyled dashboard-list">
                                                                <div className="no-data-masker-wrapper">
                                                                    <div>
                                                                        <div className="width-30 vertical-align">
                                                                            <div className="no-data-masker profile-masker">
                                                                                <div className="circle"></div>

                                                                                <div className="horizontal-bars">
                                                                                    <div className="horizontal-bar"></div>

                                                                                    <div className="horizontal-bar sub-content"></div>
                                                                                </div>
                                                                            </div>

                                                                        </div>


                                                                    </div>
                                                                    <div className="empty-msg">
                                                                        No interviews today
                                                                    </div>
                                                                </div>
                                                                <div className="infinity-scroll hide ember-view">
                                                                    <div className="no-data-masker-wrapper loading-masker">

                                                                        <div>
                                                                            <div className="width-30 vertical-align">
                                                                                <div className="no-data-masker profile-masker">
                                                                                    <div className="circle"></div>
                                                                                    <div className="horizontal-bars">
                                                                                        <div className="horizontal-bar"></div>
                                                                                        <div className="horizontal-bar sub-content"></div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div
                                                                                className="width-30 pl0 vertical-align">
                                                                                <div className="no-data-masker">
                                                                                    <div className="horizontal-bars">
                                                                                        <div
                                                                                            className="horizontal-bar"></div>
                                                                                        <div
                                                                                            className="horizontal-bar sub-content"></div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div
                                                                                className="width-30 pl0 vertical-align">
                                                                                <div className="no-data-masker">
                                                                                    <div className="horizontal-bars">
                                                                                        <div
                                                                                            className="horizontal-bar"></div>
                                                                                        <div
                                                                                            className="horizontal-bar sub-content"></div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>

                                                                        <button role="button"
                                                                                data-total-pages="[object Object]"
                                                                                className="btn btn-default btn-sm load-more-btn"
                                                                                data-ember-action=""
                                                                                data-ember-action-2740="2740">
                                                                            Load more...
                                                                        </button>
                                                                    </div>

                                                                </div>
                                                            </ul>
                                                        </div>

                                                        <div className="ps-scrollbar-x-rail"
                                                             style={{left: 0, bottom: 0}} >
                                                            <div className="ps-scrollbar-x" tabIndex="0"
                                                                 style={{left: 0, width: 0}}></div>
                                                        </div>
                                                        <div className="ps-scrollbar-y-rail"
                                                             style={{top: 0 , right:0}}>
                                                            <div className="ps-scrollbar-y" tabIndex="0"
                                                                 style={{top: 0, height: 0}}></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                    </div>
                                </div>

                                <div className="single-block mt20">
                                    <h2 className="dashboard-title">
                                        Offer to hire highlights
                                        <div id="ember1511" className="vertical-align dropdown ember-view">
                                            <a href="#" className="dashboard-dropdown dropdown-toggle ember-view">
                                                Last 30 days
                                            </a>
                                            <ul id="ember1517" role="menu"
                                                className="dropdown-menu pull-left dropdown-menu ember-view">
                                                <li><a>Today</a></li>
                                                <li><a>Last 7 days</a></li>
                                                <li className="active">
                                                    <a>Last 30 days</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </h2>
                                    <div className="row text-center">
                                        <div className="col-xs-3">
                                            <div className="white-bg min-height-130">
                                                <div className="ember-view">
                                                    <h1 className="dashboard-count">
                                                        <a>
                                                            <i className="icon-recent-offers text-success count-icon"></i> 0
                                                        </a>
                                                    </h1>
                                                    <div className="text-muted size-16"> Offers Made</div>
                                                    <div id="ember1552" className="ember-view"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xs-3">
                                            <div className="white-bg min-height-130">
                                                <div id="ember1564" className="ember-view"><h1
                                                    className="dashboard-count">
                                                    <a role="button" data-ember-action="" data-ember-action-2786="2786">
                                                        <i className="icon-calendar text-warning count-icon"></i> 0
                                                    </a>
                                                </h1>
                                                    <div className="text-muted size-16"> Offers Accepted</div>
                                                    <div id="ember1569" className="ember-view"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xs-3">
                                            <div className="white-bg min-height-130">
                                                <div id="ember1581" className="ember-view"><h1
                                                    className="dashboard-count">
                                                    <a role="button" data-ember-action="" data-ember-action-2799="2799">
                                                        <i className="icon-offer-declines text-danger count-icon"></i> 0
                                                    </a>
                                                </h1>
                                                    <div className="text-muted size-16"> Offers Declined</div>

                                                    <div id="ember1586" className="ember-view"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xs-3">
                                            <div className="white-bg min-height-130">
                                                <div id="ember1598" className="ember-view"><h1
                                                    className="dashboard-count">
                                                    <a role="button" data-ember-action="" data-ember-action-2872="2872">
                                                        <i className="icon-recent-hires text-link count-icon"></i>
                                                        5
                                                    </a>
                                                </h1>
                                                    <div className="text-muted size-16"> New Joinees</div>
                                                    <div id="ember1607" className="ember-view"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="modal-right-slide">
                                    <div className="modal-slide hide">
                                        <div className="modal-slide-content pending-decisions">
                                            <div className="border-bt filter-title">
                                                <h4 className="semi-bold mt0">Waiting For My Action</h4>
                                            </div>
                                            <button type="button" role="button" className="close filter-close"
                                                    data-ember-action="" data-ember-action-1401="1401">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            <div>
                                                <ul className="list-unstyled dashboard-list">
                                                    <div className="no-data-masker-wrapper">
                                                        <div className="no-data-masker profile-masker">
                                                            <div className="circle"></div>
                                                            <div className="horizontal-bars">
                                                                <div className="horizontal-bar"></div>
                                                                <div className="horizontal-bar sub-content"></div>
                                                            </div>
                                                        </div>

                                                        <div className="no-data-masker profile-masker">
                                                            <div className="circle"></div>
                                                            <div className="horizontal-bars">
                                                                <div className="horizontal-bar"></div>
                                                                <div className="horizontal-bar sub-content"></div>
                                                            </div>
                                                        </div>

                                                        <div className="empty-msg">
                                                            Good job! Nothing's waiting on you!
                                                        </div>
                                                    </div>
                                                    <li id="ember2722" className="infinity-scroll hide ember-view">
                                                        <div className="no-data-masker-wrapper loading-masker">
                                                            <div className="no-data-masker profile-masker">
                                                                <div className="circle"></div>
                                                                <div className="horizontal-bars">
                                                                    <div className="horizontal-bar"></div>
                                                                    <div className="horizontal-bar sub-content"></div>
                                                                </div>
                                                            </div>

                                                            <button role="button" data-total-pages="[object Object]"
                                                                    className="btn btn-default btn-sm load-more-btn"
                                                                    data-ember-action="" data-ember-action-2723="2723">
                                                                Load more...
                                                            </button>
                                                        </div>

                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-slide hide">
                                        <div data-statuses="<DS.PromiseArray:ember1423>"
                                             className="modal-slide-content my-tasks">
                                            <div className="border-bt filter-title">
                                                <h4 className="semi-bold mt0">Tasks</h4>
                                            </div>
                                            <button type="button" role="button" className="close filter-close"
                                                    data-ember-action="" data-ember-action-1428="1428">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            <div className="tasks">
                                                <div data-total-pages="[object Object]" className="tab-content">
                                                    <ul className="list-unstyled dashboard-list">
                                                        <div className="no-data-masker-wrapper">
                                                            <div className="no-data-masker profile-masker">
                                                                <div className="circle"></div>
                                                                <div className="horizontal-bars">
                                                                    <div className="horizontal-bar"></div>
                                                                    <div className="horizontal-bar sub-content"></div>
                                                                </div>
                                                            </div>

                                                            <div className="no-data-masker profile-masker">
                                                                <div className="circle"></div>
                                                                <div className="horizontal-bars">
                                                                    <div className="horizontal-bar"></div>
                                                                    <div className="horizontal-bar sub-content"></div>
                                                                </div>
                                                            </div>

                                                            <div className="no-data-masker profile-masker">
                                                                <div className="circle"></div>
                                                                <div className="horizontal-bars">
                                                                    <div className="horizontal-bar"></div>
                                                                    <div className="horizontal-bar sub-content"></div>
                                                                </div>
                                                            </div>

                                                            <div className="empty-msg">
                                                                Give yourself a pat on the back.
                                                            </div>
                                                        </div>
                                                        <div id="ember2968" className="infinity-scroll hide ember-view">
                                                            <div className="no-data-masker-wrapper loading-masker">
                                                                <div className="no-data-masker profile-masker">
                                                                    <div className="circle"></div>
                                                                    <div className="horizontal-bars">
                                                                        <div className="horizontal-bar"></div>
                                                                        <div
                                                                            className="horizontal-bar sub-content"></div>
                                                                    </div>
                                                                </div>

                                                                <button role="button" data-total-pages="[object Object]"
                                                                        className="btn btn-default btn-sm load-more-btn"
                                                                        data-ember-action=""
                                                                        data-ember-action-2969="2969">
                                                                    Load more...
                                                                </button>
                                                            </div>

                                                        </div>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-slide hide">
                                        <div className="modal-slide-content recent-offers">
                                            <div className="border-bt filter-title">
                                                <h4 className="semi-bold mt0">
                                                    Offers Made -
                                                    <span className="text-muted text-light">
              Last 30 days
          </span>
                                                </h4>
                                            </div>
                                            <button type="button" role="button" className="close filter-close"
                                                    data-ember-action="" data-ember-action-1561="1561">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            <div>
                                                <ul className="list-unstyled dashboard-list">
                                                    <div className="no-data-masker-wrapper">
                                                        <div className="width-45 vertical-align">
                                                            <div className="no-data-masker profile-masker">
                                                                <div className="circle"></div>
                                                                <div className="horizontal-bars">
                                                                    <div className="horizontal-bar"></div>
                                                                    <div className="horizontal-bar sub-content"></div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div className="width-45 pl10 vertical-align">
                                                            <div className="no-data-masker">
                                                                <div className="horizontal-bars">
                                                                    <div className="horizontal-bar"></div>
                                                                    <div className="horizontal-bar sub-content"></div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div className="width-45 vertical-align">
                                                            <div className="no-data-masker profile-masker">
                                                                <div className="circle"></div>
                                                                <div className="horizontal-bars">
                                                                    <div className="horizontal-bar"></div>
                                                                    <div className="horizontal-bar sub-content"></div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div className="width-45 pl10 vertical-align">
                                                            <div className="no-data-masker">
                                                                <div className="horizontal-bars">
                                                                    <div className="horizontal-bar"></div>
                                                                    <div className="horizontal-bar sub-content"></div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div className="width-45 vertical-align">
                                                            <div className="no-data-masker profile-masker">
                                                                <div className="circle"></div>
                                                                <div className="horizontal-bars">
                                                                    <div className="horizontal-bar"></div>
                                                                    <div className="horizontal-bar sub-content"></div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div className="width-45 pl10 vertical-align">
                                                            <div className="no-data-masker">
                                                                <div className="horizontal-bars">
                                                                    <div className="horizontal-bar"></div>
                                                                    <div className="horizontal-bar sub-content"></div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div className="empty-msg">
                                                            The right candidate is just around the corner.
                                                            <p className="size-16">
                                                                No new offers were made recently.
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div id="ember2778" className="infinity-scroll hide ember-view">
                                                        <div className="no-data-masker-wrapper loading-masker">
                                                            <div className="width-45 vertical-align">
                                                                <div className="no-data-masker profile-masker">
                                                                    <div className="circle"></div>
                                                                    <div className="horizontal-bars">
                                                                        <div className="horizontal-bar"></div>
                                                                        <div
                                                                            className="horizontal-bar sub-content"></div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div className="width-45 pl10 vertical-align">
                                                                <div className="no-data-masker">
                                                                    <div className="horizontal-bars">
                                                                        <div className="horizontal-bar"></div>
                                                                        <div
                                                                            className="horizontal-bar sub-content"></div>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <button role="button" data-total-pages="[object Object]"
                                                                    className="btn btn-default btn-sm load-more-btn"
                                                                    data-ember-action="" data-ember-action-2779="2779">
                                                                Load more...
                                                            </button>
                                                        </div>

                                                    </div>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-slide hide">
                                        <div className="modal-slide-content upcoming-joinees">
                                            <div className="border-bt filter-title">
                                                <h4 className="semi-bold mt0">
                                                    Offers Accepted -
                                                    <span className="text-muted text-light">Last 30 days</span>
                                                </h4>
                                            </div>
                                            <button type="button" className="close filter-close"
                                                    data-ember-action="" data-ember-action-1578="1578">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            <div>
                                                <ul className="list-unstyled dashboard-list">
                                                    <div className="no-data-masker-wrapper">
                                                        <div className="no-data-masker profile-masker">
                                                            <div className="circle"></div>
                                                            <div className="horizontal-bars">
                                                                <div className="horizontal-bar"></div>
                                                                <div className="horizontal-bar sub-content"></div>
                                                            </div>
                                                        </div>

                                                        <div className="no-data-masker profile-masker">
                                                            <div className="circle"></div>
                                                            <div className="horizontal-bars">
                                                                <div className="horizontal-bar"></div>
                                                                <div className="horizontal-bar sub-content"></div>
                                                            </div>
                                                        </div>

                                                        <div className="empty-msg">
                                                            The right candidate is just around the corner.
                                                        </div>
                                                    </div>
                                                    <li id="ember2791" className="infinity-scroll hide ember-view">
                                                        <div className="no-data-masker-wrapper loading-masker">
                                                            <div className="no-data-masker profile-masker">
                                                                <div className="circle"></div>
                                                                <div className="horizontal-bars">
                                                                    <div className="horizontal-bar"></div>
                                                                    <div className="horizontal-bar sub-content"></div>
                                                                </div>
                                                            </div>

                                                            <button role="button" data-total-pages="[object Object]"
                                                                    className="btn btn-default btn-sm load-more-btn"
                                                                    data-ember-action="" data-ember-action-2792="2792">
                                                                Load more...
                                                            </button>
                                                        </div>

                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-slide hide">
                                        <div className="modal-slide-content offer-declines">
                                            <div className="border-bt filter-title">
                                                <h4 className="semi-bold mt0">
                                                    Offers Declined -
                                                    <span className="text-muted text-light">
              Last 30 days
          </span>
                                                </h4>
                                            </div>
                                            <button type="button" role="button" className="close filter-close"
                                                    data-ember-action="" data-ember-action-1595="1595">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            <div>
                                                <ul className="list-unstyled dashboard-list">
                                                    <div className="no-data-masker-wrapper">
                                                        <div className="no-data-masker profile-masker">
                                                            <div className="circle"></div>
                                                            <div className="horizontal-bars">
                                                                <div className="horizontal-bar"></div>
                                                                <div className="horizontal-bar sub-content"></div>
                                                            </div>
                                                        </div>

                                                        <div className="no-data-masker profile-masker">
                                                            <div className="circle"></div>
                                                            <div className="horizontal-bars">
                                                                <div className="horizontal-bar"></div>
                                                                <div className="horizontal-bar sub-content"></div>
                                                            </div>
                                                        </div>

                                                        <div className="empty-msg">
                                                            When candidates decline your offer, they go here.
                                                        </div>
                                                    </div>
                                                    <li id="ember2804" className="infinity-scroll hide ember-view">
                                                        <div className="no-data-masker-wrapper loading-masker">
                                                            <div className="no-data-masker profile-masker">
                                                                <div className="circle"></div>
                                                                <div className="horizontal-bars">
                                                                    <div className="horizontal-bar"></div>
                                                                    <div className="horizontal-bar sub-content"></div>
                                                                </div>
                                                            </div>

                                                            <button role="button" data-total-pages="[object Object]"
                                                                    className="btn btn-default btn-sm load-more-btn"
                                                                    data-ember-action="" data-ember-action-2805="2805">
                                                                Load more...
                                                            </button>
                                                        </div>

                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-slide hide">
                                        <div className="modal-slide-content recent-hires">
                                            <div className="border-bt filter-title">
                                                <h4 className="semi-bold mt0">
                                                    New Joinees -
                                                    <span className="text-muted text-light">
              Last 30 days
          </span>
                                                </h4>
                                            </div>
                                            <button type="button" role="button" className="close filter-close"
                                                    data-ember-action="" data-ember-action-1616="1616">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            <div className="">
                                                <ul className="list-unstyled dashboard-list">
                                                    <li>
                                                        <a href="/employees/2000016116" id="ember2874"
                                                           className="ember-view">
                                                            <div id="ember2881" className="ember-view">
                                                                <div className="avatar-circle">
                                                                    <div id="ember2882"
                                                                         className="initialsAvatar avatarColor-15 ember-view"> C
                                                                    </div>
                                                                </div>
                                                                <div className="vertical-align profile-details">
                                                                    <div className="name ellipsis"> Corina Willie
                                                                        (sample)
                                                                    </div>
                                                                    <div className="text-muted ellipsis">VP
                                                                        Admin &amp; Operations
                                                                    </div>
                                                                     </div>
                                                            </div>
                                                        </a>
                                                        <div className="recent-offer-info">
                                                            Joined
                                                            <span className="text-color"> Operations</span> team,
                                                            4 days ago
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <a href="/employees/2000016117" id="ember2892"
                                                           className="ember-view">
                                                            <div id="ember2893" className="ember-view">
                                                                <div className="avatar-circle">
                                                                    <div id="ember2894"
                                                                         className="initialsAvatar avatarColor-15 ember-view"> D
                                                                    </div>
                                                                </div>
                                                                <div className="vertical-align profile-details">
                                                                    <div className="name ellipsis"> Dreda Mikki
                                                                        (sample)
                                                                    </div>
                                                                    <div className="text-muted ellipsis">Sales Manager
                                                                    </div>
                                                                    </div>
                                                            </div>
                                                        </a>
                                                        <div className="recent-offer-info">
                                                            Joined
                                                            <span className="text-color"> Customer Success</span> team,
                                                            4 days ago
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <a href="/employees/2000016118" id="ember2904"
                                                           className="ember-view">
                                                            <div id="ember2905" className="ember-view">
                                                                <div className="avatar-circle">
                                                                    <div id="ember2906"
                                                                         className="initialsAvatar avatarColor-5 ember-view"> L
                                                                    </div>
                                                                </div>
                                                                <div className="vertical-align profile-details">
                                                                    <div className="name ellipsis"> Langdon Brennan
                                                                        (sample)
                                                                    </div>
                                                                    <div className="text-muted ellipsis">HR Manager
                                                                    </div>
                                                                     </div>
                                                            </div>
                                                        </a>
                                                        <div className="recent-offer-info">
                                                            Joined
                                                            <span
                                                                className="text-color"> Talent Acquisition</span> team,
                                                            4 days ago
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <a href="/employees/2000016114" id="ember2916"
                                                           className="ember-view">
                                                            <div id="ember2917" className="ember-view">
                                                                <div className="avatar-circle">
                                                                    <div id="ember2918"
                                                                         className="initialsAvatar avatarColor-21 ember-view"> P
                                                                    </div>
                                                                </div>
                                                                <div className="vertical-align profile-details">
                                                                    <div className="name ellipsis"> patilmantu99
                                                                        patilmantu99
                                                                    </div>
                                                                    <div className="text-muted ellipsis"></div>
                                                                  </div>
                                                            </div>
                                                        </a>
                                                        <div className="recent-offer-info">
                                                            Joined
                                                            <span className="text-color"> Business</span> team,
                                                            4 days ago
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <a href="/employees/2000016115" id="ember2928"
                                                           className="ember-view">
                                                            <div id="ember2929" className="ember-view">
                                                                <div className="avatar-circle">
                                                                    <div id="ember2930"
                                                                         className="initialsAvatar avatarColor-23 ember-view"> S
                                                                    </div>
                                                                </div>
                                                                <div className="vertical-align profile-details">
                                                                    <div className="name ellipsis"> Stuart Errol
                                                                        (sample)
                                                                    </div>
                                                                    <div className="text-muted ellipsis">VP Sales and
                                                                        Marketing
                                                                    </div>
                                                                    </div>
                                                            </div>
                                                        </a>
                                                        <div className="recent-offer-info">
                                                            Joined
                                                            <span className="text-color"> Business</span> team,
                                                            4 days ago
                                                        </div>
                                                    </li>
                                                    <div id="ember2939" className="infinity-scroll hide ember-view">
                                                        <div className="no-data-masker-wrapper loading-masker">
                                                            <div className="no-data-masker profile-masker">
                                                                <div className="circle"></div>
                                                                <div className="horizontal-bars">
                                                                    <div className="horizontal-bar"></div>
                                                                    <div className="horizontal-bar sub-content"></div>
                                                                </div>
                                                            </div>

                                                            <button role="button" data-total-pages="[object Object]"
                                                                    className="btn btn-default btn-sm load-more-btn"
                                                                    data-ember-action="" data-ember-action-2940="2940">
                                                                Load more...
                                                            </button>
                                                        </div>

                                                    </div>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
        </div>





      );
  }
}

export default Interviews_for_me;
