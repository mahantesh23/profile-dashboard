/* Displays The candidate Summary by fetching data from flask API */

import React, { Component } from 'react';

import './Nav.css';

class Candidate_summary extends Component {
    state = {
    products: []
  }

  componentDidMount(){
    this.getProfiles();
  }

  //Fetching the records from Database from API using fetch function.
  getProfiles = _=>{
    fetch('http://localhost:8080/profiles')
    .then(response => response.json())
    .then(response => this.setState({
        products:response
    }))
    .catch(err => console.error(err))
  }

  //renderProduct holds records by specifying columns names of table
        renderNewCandidate = ({id, new_Candidate}) =>
      <a key={id}> {new_Candidate} </a>;
        renderResponded = ({id, responded}) =>
      <a key={id}> {responded} </a>;
        renderWaiting = ({id, waiting}) =>
      <a key={id}> {waiting} </a>;
        renderDueTask = ({id, due_task}) =>
      <a key={id}> {due_task} </a>;



  render() {
       const { products } =this.state;

    return (
                         <div className="content-wrapper">
                                    <div className="ember-view">
                                        <div className="pagearea dashboard ember-view">
                                            <div className="pagearea-content clearfix ">
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <div className="single-block ember-view">
                                                            <h2 className="dashboard-title">Candidates summary</h2>
                                                            <div className="row text-center">
                                                                <div className="col-xs-3">
                                                                    <div className="white-bg min-height-130">
                                                                        <h1 className="dashboard-count">
                                                                            <a href="/hire/leads?unread=true" className="ember-view">
                                                                                <i className="icon-new-cand text-success count-icon"></i> {products.map(this.renderNewCandidate)}
                                                                            </a>
                                                                        </h1>

                                                                        <div className="text-muted size-16">New Candidates</div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-xs-3">
                                                                    <div className="white-bg min-height-130">
                                                                        <h1 className="dashboard-count">
                                                                            <a href="/conversations?awaiting_response=true" className="ember-view">
                                                                                <strong className="text-danger count-icon">!&nbsp;
                                                                                    <i className="icon-email"></i></strong>&nbsp;{products.map(this.renderResponded)}
                                                                            </a>
                                                                        </h1>

                                                                        <div className="text-muted size-16">Candidates Responded</div>
                                                                    </div>
                                                                </div>
                                                                <div className="col-xs-3">
                                                                    <div className="white-bg min-height-130">
                                                                        <div  className="ember-view"><h1
                                                                            className="dashboard-count">
                                                                            <a role="button" data-ember-action=""
                                                                               data-ember-action-2717="2717">
                                                                                <i className="icon-waiting-for-decision text-warning count-icon"></i>  {products.map(this.renderWaiting)}
                                                                            </a>
                                                                        </h1>

                                                                            <div className="text-muted size-16"> Waiting For My Action
                                                                            </div>
                                                                            <div  className="ember-view"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="col-xs-3">
                                                                    <div className="white-bg min-height-130">
                                                                        <div  className="ember-view"><h1
                                                                            className="dashboard-count">
                                                                            <a role="button" data-ember-action=""
                                                                               data-ember-action-2963="2963"><i
                                                                                className="icon-task text-link count-icon"></i> </a>{products.map(this.renderDueTask)}
                                                                        </h1>

                                                                            <div className="text-muted size-16">Due tasks</div>
                                                                            <div  className="ember-view"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                         </div>

    );
  }
}
 export default Candidate_summary;